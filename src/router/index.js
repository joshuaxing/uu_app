import Vue from 'vue'
import Router from 'vue-router'
import store from '../vuex/index'
Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: resolve => require(['@/components/page/Index'], resolve)
    },
    {
      path: '/zyhq',
      name: 'zyhq',
      component: resolve => require(['@/components/page/Zyhq'], resolve)
    },
    {
      path: '/hqxq',
      name: 'hqxq',
      component: resolve => require(['@/components/page/Hqxq'], resolve)
    },
    {
      path: '/case',
      name: 'case',
      component: resolve => require(['@/components/page/Case'], resolve)
    },
    {
      path: '/test',
      name: 'test',
      component: resolve => require(['@/components/test'], resolve)
    }
  ]
})
router.beforeEach((to, from, next) => {
 
  if (to.matched.some(record => record.meta.requiresAuth)) {
    next()
  } else {
    next() // 确保一定要调用 next()
  }
})

export default router;