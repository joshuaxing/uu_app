// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from './axios/http'
import store from './vuex'
import 'lib-flexible'
import './assets/scss/index.scss'
import VueLazyload from 'vue-lazyload'
import fastclick from 'fastclick'
import Cube from 'cube-ui'
//fastclick.attach(document.body)
Vue.prototype.axios = axios
Vue.config.productionTip = false
Vue.use(VueLazyload)
Vue.use(Cube)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  axios,
  store,
  components: { App },
  template: '<App/>'
})

