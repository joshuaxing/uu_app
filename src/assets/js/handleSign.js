import md5 from 'blueimp-md5';
export default function createSign (data) {
  var arr = [];
  for (var key in data) {
    arr.push(key+''+data[key])
  }
  arr.sort();
  var str = '';
  for (var i=0; i< arr.length; i++) {
    str += arr[i]
  }
  str = md5('ztznuythj2018qweiyhjkhfvbmzvnmrk'+str)
  var app_sign_b = stringToByte(str);
  var app_sign = bin2String(app_sign_b).toUpperCase();
  return app_sign
}

function stringToByte (str) {
  var bytes = new Array();
  var len, c;
  len = str.length;
  for(var i = 0; i < len; i++) {
    c = str.charCodeAt(i);
    if(c >= 0x010000 && c <= 0x10FFFF) {
      bytes.push(((c >> 18) & 0x07) | 0xF0);
      bytes.push(((c >> 12) & 0x3F) | 0x80);
      bytes.push(((c >> 6) & 0x3F) | 0x80);
      bytes.push((c & 0x3F) | 0x80);
    } else if(c >= 0x000800 && c <= 0x00FFFF) {
      bytes.push(((c >> 12) & 0x0F) | 0xE0);
      bytes.push(((c >> 6) & 0x3F) | 0x80);
      bytes.push((c & 0x3F) | 0x80);
    } else if(c >= 0x000080 && c <= 0x0007FF) {
      bytes.push(((c >> 6) & 0x1F) | 0xC0);
      bytes.push((c & 0x3F) | 0x80);
    } else {
      bytes.push(c & 0xFF);
    }
  }
  return bytes;
}
function bin2String (array) {
  return String.fromCharCode.apply(String, array)
}