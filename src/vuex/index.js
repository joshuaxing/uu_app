import Vue from 'vue'
import Vuex from 'vuex'
import door from './modules/door'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    door
  },
  getters: {
    storeId (state, getters) {
      return state.door.storeId
    },
  },
  mutations: {
    
  },
  actions: {

  }
})

export default store
