const getters = {
  goodsData: state => state.goods.goodsData,
  userID: state => state.user.userID
}
export default getters
