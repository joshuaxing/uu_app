const door = {
  state: {
    storeId: null,
    storeName: null
  },
  mutations: {
    gotStore (state, payload) {
      state.storeId = Number(payload.value.storeId);
      state.storeName = payload.value.storeName;
    }
  },
  actions: {

  }
}

export default door