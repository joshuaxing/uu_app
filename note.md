###多行文本溢出
```
width: 100px;
overflow: hidden;
text-overflow: ellipsis;
display: -webkit-box;
-webkit-line-clamp: 3; //行数
-webkit-box-orient: vertical;
```
###单行文本溢出
```
width: 100px;
white-space: nowrap;
overflow: hidden;
text-overflow: ellipsis;
```
###css下三角形
```
width: 0;
height: 0;
border: 5px solid transparent;
border-top-color: #e94653;
```
###css上三角形
```
width: 0;
height: 0;
border: 5px solid transparent;
border-bottom-color: #e94653;
```
###CSS实现折叠面板
####浏览器地址的hash(地址里#号后面的部分)和:target伪选择器指定的ID匹配上时，它的样式就会在ID元素上生效
```
<a href="#link1">Link 1</a>
<div id="link1">
  <h3>Content to Link 1</h3>
  <p>Hello World!</p>
</div>
<style>
div {
  display: none;
}
div:target {
  display: block;
}
</style>
```